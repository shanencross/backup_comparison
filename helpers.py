def store_backup_input(backup_filepath, backup):
    """Store data from backup file to a dictionary."""

    with open(backup_filepath) as backup_file:
        for line in backup_file:
            add_line_to_backup(line, backup)

def write_output(output_path, output_dict):
    """Write data from backup dictionary to a file."""
    
    with open(output_path, "w") as output_file:
        for filename in output_dict.values():
            output_file.write(filename + "\n")

def add_line_to_backup(line, backup):
    """Store file data from single backup file line to a dictionary."""

    # Remove trailing newline.
    stripped_line = line.rstrip()

    # The file key is everything before the first space.
    # Everything after is the filename.
    split_line = stripped_line.split(sep= " ", maxsplit=1)
    key = split_line[0]
    filename  = split_line[1]
    backup[key] = filename