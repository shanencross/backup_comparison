import random
import sys
import os

def main():
    """# Randomly generate test input for backup comparison."""
    
    if len(sys.argv) >= 2:
        file_count = int(sys.argv[1])
    else:
        file_count = 5

    backup_dir = "./test_files/test_random"
    os.makedirs(backup_dir, exist_ok=True)

    old_backup_path = os.path.join(backup_dir, "Old.sha1.txt")
    new_backup_path = os.path.join(backup_dir, "New.sha1.txt")

    with open(old_backup_path, "w") as old_backup_file:
        write_randomized_backup_file(old_backup_file, file_count)

    with open(new_backup_path, "w") as new_backup_file:
        write_randomized_backup_file(new_backup_file, file_count)

def write_randomized_backup_file(backup_file, file_count):
    # We select keys from a sample of integers twice as large as the number of files.
    # This way, there's probability that 50% of them will be shared between the old
    # backup and the new backup.

    # You can change this to tweak the likelihood of shared keys.
    max_random_int = int(file_count * 2)
    keys = random.sample(range(0, max_random_int), file_count)

    for key in keys:
            file_name_num = random.randrange(0, max_random_int)
            file_name = "file " + str(file_name_num) + ".txt"
            line = str(key) + " " + file_name
            backup_file.write(line + "\n")

if __name__ == "__main__":
    main()