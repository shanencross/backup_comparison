import time
import sys
import backup_comparison

# If turned on, execution time will be measured and printed.
debug = False

def main():
    """Compare old backup and new backup given via command line arguments."""

    # get command line arguments
    if len(sys.argv) <= 2:
        print("Enter two filepath arguments, or zero arguments for default paths.")
        return
    else:
        old_backup_filepath = sys.argv[1]
        new_backup_filepath = sys.argv[2]
    
    backup_comparison.compare_backups(old_backup_filepath, new_backup_filepath)

if __name__ == "__main__":
    if debug:
        start_time = time.perf_counter()
    
    main()
   
    if debug:
        print("--- %s seconds ---" % (time.perf_counter() - start_time))