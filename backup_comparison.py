import sys
import os
import helpers

def compare_backups(old_backup_filepath, new_backup_filepath):
    """Compare old backup and new backup.
    Generates one backup listing files in old backup but not in new backup,
    and another backup listing files in new backup but not in old backup.
    """
    
    # create output folder and set output filepaths
    results_dir = os.path.join(sys.path[0], "results")
    os.makedirs(results_dir, exist_ok=True)
    old_not_in_new_filepath = os.path.join(results_dir, "OldNotInNew.txt")
    new_not_in_old_filepath = os.path.join(results_dir, "NewNotInOld.txt")

    # read input backup files and store data in dictionaries
    old_backup = {}
    new_backup = {}
    helpers.store_backup_input(old_backup_filepath, old_backup)
    helpers.store_backup_input(new_backup_filepath, new_backup)
    
    # construct dictionaries for old files not in new backup, and new files not in old backup
    old_not_in_new = {key: old_backup[key] for key in old_backup if key not in new_backup}
    new_not_in_old = {key: new_backup[key] for key in new_backup if key not in old_backup}

    # write output to file
    helpers.write_output(old_not_in_new_filepath, old_not_in_new)
    helpers.write_output(new_not_in_old_filepath, new_not_in_old)