# Instructions

* Install Python 3 on your OS: https://www.python.org/

  Software was developed and tested with Python 3.7.3 on Windows 10.

* Navigate to your workspace and clone this repository:
  ```
  git clone https://gitlab.com/shanencross/backup_comparison.git
  ```

* Navigate your console to the top folder of the repo:
  ```
  cd backup_comparison
  ```

* Run main.py with Python 3 with two additional arguments. The first argument should be the path to the old backup text file, and the second argument should be the path to the new backup text file.

  For example, assuming your Python 3 installation has been added to your system PATH:
  ```
  python main.py Old.sha1.txt New.sha1.txt
  ```

* The output will be generated in the subfolder /results as OldNotInNew.txt and NewNotInOld.txt.

# Psuedo-Input Generation (Optional Testing Tool)

I used the input_generation.py script to randomly generate psuedo-input for the program for testing.

I say "pseudo"-input, because it generates random integers in place of actual sha1 keys, so it's not exactly what we expect real input to look like. 

But it's close, and can be used to quickly generate large backup files for comparison.

By default, keys are randomly selected from a pool of integers

Here are instructions for it:

* To generate the backups, navigate to the testing/ folder. If navigating from the project root:
  ```
  cd testing
  ```

* Run input_generation.py with one additional argument. The argument is the number of files to generate in each backup.

  For example, to generate backups with 500 files each:
  ```
  python input_generation.py 500
  ```

* The results will be stored in the directory ./test_files/test_random (relative to where you ran the Python file from) with the filenames Old.sha1.txt and New.sha1.txt.